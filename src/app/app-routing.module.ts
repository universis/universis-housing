import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './layouts/index.component';
import { AuthGuard } from '@universis/common';


const routes: Routes = [
  {
    path: '',
    redirectTo: 'housing',
    pathMatch: 'full',
  },
  {
    path: '',
    component: IndexComponent,
    canActivateChild: [
      AuthGuard
    ],
    children: [
      {
        path: 'home',
        loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
      },
      {
        path: 'housing',
        loadChildren: () => import('@universis/ngx-housing/admin').then( m => m.AdminHousingModule )
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
