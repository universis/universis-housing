// tslint:disable: trailing-comma
export const APP_SIDEBAR_LOCATIONS = [
  {
    name: 'Home',
    key: 'Sidebar.Dashboard',
    url: '/housing/index',
    icon: 'fa fa-archive',
    index: 0
  },
  {
    name: 'HousingCards',
    key: 'Sidebar.Dashboard',
    url: '/housing/housing-cards',
    icon: 'fa fa-id-card',
    index: 1
  },
  {
    name: 'Messages',
    key: 'Sidebar.Dashboard',
    url: '/housing/messages',
    icon: 'fa fa-envelope',
    index: 2
  }
];
